#!/bin/bash

export TARGET=android-arm64
export _LIB_DIR=libs

export DST_TOOLCHAIN_PATH=${TARGET}
export ANDROID_NDK_HOME=$(pwd)
export ANDROID_NDK_ROOT=$(pwd)
export toolchains_path=$(python toolchains_path.py --ndk ${ANDROID_NDK_HOME})
# Set compiler clang, instead of gcc by default
export CC=clang
# Set the Android API levels
export ANDROID_API=21
# Set the target architecture
# Can be android-arm, android-arm64, android-x86, android-x86 etc
export CROSS_COMPILE=aarch64-linux-android
export CC=aarch64-linux-android26-clang
export architecture=android-arm64
# Add toolchains bin directory to PATH
export PATH=$toolchains_path/bin:$PATH
OUTPUT=$ANDROID_NDK_HOME/built-libs/
OUTPUT_INCLUDE=$ANDROID_NDK_HOME/built-libs/include
OUTPUT_LIB=$ANDROID_NDK_HOME/built-libs/lib/${architecture}
mkdir -p $OUTPUT_INCLUDE
mkdir -p $OUTPUT_LIB

wget "https://gist.githubusercontent.com/lixiaoyi/8096570c0d0970ea6a86fa689259abc1/raw/c5fb361cc8050fbb2d7b256c1fac4f1491d3bebc/toolchains_path.py"

mkdir ${_LIB_DIR}
cd ${_LIB_DIR}

cd zlib
./configure --prefix=${OUTPUT}
make clean
make -j${_NUM_CPU}
make install

cd ${_LIB_DIR}

cd openssl
./Configure ${TARGET} --prefix=${OUTPUT} -D__ANDROID_API__=$ANDROID_API
make clean
make -j${_NUM_CPU}
make install
cp -RL include/openssl $OUTPUT_INCLUDE
cp libcrypto.so $OUTPUT_LIB
cp libcrypto.a $OUTPUT_LIB
cp libssl.so $OUTPUT_LIB
cp libssl.a $OUTPUT_LIB

cd ${_LIB_DIR}

cd curl
autoreconf -fi
./configure --prefix=${OUTPUT} --target=${TARGET} --with-zlib=${OUTPUT} --with-ssl=${OUTPUT} --host=x86_64-pc-linux-gnu  --disable-ldap --disable-ldaps --disable-rtsp --disable-telnet --disable-proxy --disable-tftp --disable-pop3 --disable-imap --disable-rtsp --disable-smb --disable-smtp --disable-gopher --disable-mqtt --disable-manual --disable-threaded-resolver --disable-pthreads --disable-ntlm --disable-ntlm-wb --disable-doh --disable-dnsshuffle --disable-hsts --disable-websockets --without-huper --without-brotli --without-zstd --without-libpsl --without-librtmp --without-winidn --without-libidn2 --without-quiche --without-msh3 
make clean
make -j${_NUM_CPU}
make install

cd ${_LIB_DIR}

cd libarchive/build
./autogen.sh --prefix=${OUTPUT} --host=aarch64-linux-android --target=${architecture} 
cd ..
./configure  --prefix=${OUTPUT} --host=aarch64-linux-android --target=${architecture} --without-xml2
cp -va contrib/android/include/* libarchive/
make clean
make -j${_NUM_CPU}
make install

cd ${_LIB_DIR}

cd json-c
if [ ! -d "build" ]; then
	mkdir build
else
	rm -r build/*
fi
cd build
../cmake-configure --prefix=${OUTPUT}
make -j${_NUM_CPU}
make install

cd ${_LIB_DIR}

cd libusb
./autogen.sh  --prefix=${OUTPUT} --host=x86_64-pc-linux-gnu --enable-udev=no
./configure  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} --enable-udev=no
make -j${_NUM_CPU}
sudo make install

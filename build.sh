#!/bin/bash

#apt install unp sudo pkg-config cmake unp

_PWD=$(pwd)
_NDK_VERSION=android-ndk-r25b
_CT_DIR=${_PWD}/crosstool-ng
_LIB_DIR=${_PWD}/libs
_OUT_DIR=${_CT_DIR}/outDir
_FINAL_DST_DIR=${_PWD}/output
export TARGET=${1}

if [ -z $3 ]; then
	_GLIBC_VERSION=2_35
else
	_GLIBC_VERSION=$3
fi

if [ -z $4 ]; then
	_USE_UCLIBC=0
	echo "################################"
	echo "Glibc version: ${_GLIBC_VERSION}"
	echo "################################"
else
	_USE_UCLIBC=1
	echo "################################"
	echo "Using uClibc"
	echo "################################"
fi
_OPENSSL_TARGET=$2

_NUM_CPU=$(expr $(cat /proc/cpuinfo | grep processor | wc -l) - 2)



function makeDirs() {
	if [ ! -d "libs" ]; then
		mkdir libs
	fi

	if [ ! -d "output" ]; then
		mkdir output
	fi

	if [ "${TARGET}" != "android_x64" ]; then
		if [ ! -d "${_OUT_DIR}" ]; then
			mkdir ${_OUT_DIR}
		fi	
	fi
	
	if [ ! -d "libs" ]; then
		mkdir libs
	fi
	
	if [ ! -d "${_LIB_DIR}" ]; then
		mkdir "${_LIB_DIR}"
	fi
	
	
	
}

function getSources () {
	if [ "${TARGET}" != "android_x64" ]; then
		if [ ! -d "crosstool-ng" ]; then
			echo "[+] Downloading crosstool-ng source"
			git clone https://github.com/crosstool-ng/crosstool-ng.git
			cd ${_CT_DIR}
			echo "[+] Bootstrapping"
			./bootstrap
			echo "[+] Running Configure"
			./configure --enable-local --prefix=/usr/local --host=x86_64-pc-linux-gnu
			if [ $? -ne 0 ]; then
				echo "Error configuring crosstool"
				exit
			fi
			echo "[+] Make"
			make -j${_NUM_CPU}
			if [ $? -ne 0 ]; then
				echo "Error building crosstool"
				exit
			fi
			echo "[+] Installing"
			sudo make install
		fi
	
		cd ${_PWD}
	fi
	
	
	if [ "${TARGET}" == "android_x64" ]; then
		if [ ! -d "${_NDK_VERSION}" ]; then
			echo "[+] Downloading NDK v${_NDK_VERSION}"
			wget "https://dl.google.com/android/repository/${_NDK_VERSION}-linux.zip?hl=es-419" -O ndk.zip
			unp ndk.zip &>/dev/null
			rm ndk.zip
			cd android-ndk-r25b
			echo "[+] Copying toolchain_path script"
			cp -a ../toolchains_path.py .
			#wget "https://gist.githubusercontent.com/lixiaoyi/8096570c0d0970ea6a86fa689259abc1/raw/c5fb361cc8050fbb2d7b256c1fac4f1491d3bebc/toolchains_path.py"
		fi
	fi
	#if [ ! -d "output/mipsel-buildroot-linux-uclibc" ]; then
	#	echo "[+] Downloading mips32el Toolchain"
	#	wget "https://toolchains.bootlin.com/downloads/releases/toolchains/mips32el/tarballs/mips32el--uclibc--stable-2022.08-1.tar.bz2"
	#	unp mips32el--uclibc--stable-2022.08-1.tar.bz2 &>/dev/null
	#	rm mips32el--uclibc--stable-2022.08-1.tar.bz2
	#	mv mips32el--uclibc--stable-2022.08-1 output/mipsel-buildroot-linux-uclibc
	#fi
	
	makeDirs
	
	cd ${_LIB_DIR}
	
	#Descargamos fuentes
	if [ ! -d zlib ]; then
		echo "[+] Downloading zlib"
		git clone https://github.com/madler/zlib.git	
	fi
	if [ ! -d openssl ]; then
		echo "[+] Downloading openssl"
		git clone https://github.com/openssl/openssl.git -b openssl-3.0.5 openssl
	fi
	
	if [ ! -d openssl_1_1_1 ]; then
		echo "[+] Downloading openssl 1.1.1"
		git clone https://github.com/openssl/openssl.git -b OpenSSL_1_1_1-stable openssl_1_1_1
	fi
	
	if [ ! -d curl ]; then
		echo "[+] Downloading curl"
		git clone https://github.com/curl/curl.git
	fi
	if [ ! -d libnl ]; then
		echo "[+] Downloading libnl"
		git clone https://github.com/tgraf/libnl
		echo "\t[+] Applying patches"
		cd libnl
		cp -va ${_PWD}/name_max.patch .
		git apply name_max.patch
		cd ${_LIB_DIR}
	fi
	if [ ! -d libarchive ]; then
		echo "[+] Downloading libarchive"
		git clone https://github.com/libarchive/libarchive.git
	fi
	if [ ! -d "json-c" ]; then
		echo "[+] Downloading json-c"
		git clone https://github.com/json-c/json-c.git
	fi
	if [ ! -d "nDPI" ]; then
		echo "[+] Downloading nDPI"
		git clone https://github.com/ntop/nDPI.git
	fi
	
	if [ ! -d "libusb" ]; then
		echo "[+] Downloading libUSB"
		git clone https://github.com/libusb/libusb.git
	fi
	
	if [ ! -d "zip" ]; then
		echo "[+] Downloading libZIP"
		git clone https://github.com/kuba--/zip.git
	fi
	
	if [ ! -d "gsl" ]; then
		echo "[+] Downloading libGLS"
		git clone https://github.com/ampl/gsl.git
	fi
	
	cd ${_PWD}
}

function buildMIPSEL_uClibc () {
	export DST_TOOLCHAIN_PATH=${TARGET}
	export DSTDIR="${_FINAL_DST_DIR}/${TARGET}/mipsel-buildroot-linux-uclibc/sysroot/"
	
	cd ${_FINAL_DST_DIR}/${DST_TOOLCHAIN_PATH}
	unset CC
	unset CPATH
	
	echo -e "[+] Let's Build libraries\r\n"
	cd ${_LIB_DIR}

	echo -e "[+] zlib\r\n"
	cd zlib
	./configure --prefix=${DSTDIR}
	make clean
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	cd openssl_1_1_1
	#en mips hay que desactivar los threads con no-threads
	./Configure ${_OPENSSL_TARGET} --prefix=${DSTDIR}/include no-shared zlib-dynamic no-threads -I${DSTDIR}/include 
	make clean
	make -j${_NUM_CPU} CC="${cross}gcc" AR="${cross}ar" RANLIB="${cross}ranlib"
	make install
	
	cd ${_LIB_DIR}
	
	echo -e "[+] curl\r\n"
	cd curl
	autoreconf -fi
	#--disable-threads
	./configure --prefix=${DSTDIR}/include  --target=${TARGET} --host=x86_64-pc-linux-gnu --with-zlib=${DSTDIR} --with-ssl=${DSTDIR} --enable-debug --disable-optimize --disable-symbol-hiding --disable-ftp --disable-ldap --disable-ldaps --disable-rtsp --disable-telnet --disable-proxy --disable-tftp --disable-pop3 --disable-imap --disable-rtsp --disable-smb --disable-smtp --disable-gopher --disable-mqtt --disable-manual --disable-threaded-resolver --disable-pthreads --disable-ntlm --disable-ntlm-wb --disable-doh --disable-dnsshuffle --disable-hsts --disable-websockets --without-huper --without-brotli --without-zstd --without-libpsl --without-librtmp --without-winidn --without-libidn2 --without-quiche --without-msh3 
	make clean
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] libnl\r\n"
	cd libnl
	./autogen.sh
	./configure --prefix=${DSTDIR}/include --host=x86_64-pc-linux-gnu
	make clean
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] libarchive\r\n"
	cd libarchive/build
	./autogen.sh --prefix=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} 
	cd ..
	./configure  --prefix=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --without-xml2 --without-bz2lib --without-libb2 --without-iconv --without-lz4 --without-zstd --without-lzma --without-cng --with-mbedtls --with-nettle --without-openssl
	make clean
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] json-c\r\n"
	cd json-c
	if [ ! -d "build" ]; then
		mkdir build
	fi
	cd build
	#clean previous content
	#We need to check this because the script is launched with -e
	if [ $(ls | wc -l) -ne 0 ]; then
		rm -r *
	fi
	../cmake-configure --prefix=${DSTDIR}/include
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] nDPI\r\n"
	cd nDPI
	./autogen.sh  --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --with-only-libndpi
	./configure  --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --with-only-libndpi
	make clean
	make -j${_NUM_CPU}
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] Zip\r\n"
	cd zip
	if [ ! -d "build" ]; then
		mkdir build
	fi
	cd build
	#hay que agregar esto al cmake para mips
	#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-type-limits")
	#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-type-limits")
	cmake -DBUILD_SHARED_LIBS=true -DCMAKE_INSTALL_PREFIX=${DSTDIR} ..
	cmake --build .
	make install
	
	echo -e "[+] Toolchain available at ${_FINAL_DST_DIR}\r\n"
	cd ${_PWD}
	
}


function buildCrossTool () {
	cd ${_CT_DIR}
	
	makeDirs
	
	export TARGET=$1
	export DST_TOOLCHAIN_PATH=${TARGET}
	export DSTDIR="${_OUT_DIR}/$TARGET"
	
	OUTPUT_INCLUDE=${DSTDIR}/built-libs/include
	OUTPUT_LIB=${DSTDIR}/built-libs/lib/${architecture}
	mkdir -p $OUTPUT_INCLUDE
	mkdir -p $OUTPUT_LIB
	
	unset CC
	unset CPATH
	
	#export PATH=$PATH:$(pwd)/bin
	#export cross=${TARGET}-
	#export CC="${cross}gcc"
	
	
	
	
	#Limpiamos
	if [ -d ${_OUT_DIR} ]; then
		rm -r ${_OUT_DIR}
	fi
	if [ -d ${_CT_DIR}/.build/${TARGET} ]; then
		rm -r ${_CT_DIR}/.build/${TARGET}
	fi
	
	#Configuramos el target
	echo -e "[+] Configuring Target"
	./ct-ng ${TARGET}
	if [ $? -ne 0 ]; then
		echo "Error configuring crosstool for ${TARGET}"
		exit
	fi
	
	#Aplicamos nuestra configuracion
	echo -e "[+] Applying custom configuration"
	./ct-ng savedefconfig
	if [ ! -z ${_USE_UCLIBC} ]; then
		echo -e "CT_EXPERIMENTAL=y\nCT_ALLOW_BUILD_AS_ROOT=y\nCT_ALLOW_BUILD_AS_ROOT_SURE=y\nCT_GLIBC_V_${_GLIBC_VERSION}=y\nCT_CONFIGURE_has_static_link=y" >> defconfig
	else
		echo "Using UCLIBC"
		echo -e "CT_EXPERIMENTAL=y\nCT_ALLOW_BUILD_AS_ROOT=y\nCT_ALLOW_BUILD_AS_ROOT_SURE=y\nCT_LIBC_GLIBC=n\nCT_LIBC=\"uClibc-ng\"\nCT_CONFIGURE_has_static_link=y" >> defconfig
	fi
	./ct-ng defconfig

	#Al turron!
	echo -e "[+] Building"
	./ct-ng build
	if [ $? -ne 0 ]; then
		echo "Error building crosstool for ${TARGET}"
		exit
	fi
	
	mkdir -p ${DSTDIR}/usr/local/share ${DSTDIR}/usr/local/include
	#Exportamos el path
	export PATH=$PATH:${_CT_DIR}/.build/${TARGET}/buildtools/bin/
	export cross=${TARGET}-
	export CC="${cross}gcc"
	
	echo "[+] Crosstool built"
	echo -e "[+] Let's Build libraries\r\n"
	cd ${_LIB_DIR}

	echo -e "[+] zlib\r\n"
	cd zlib
	./configure --prefix=${DSTDIR}
	if [ $? -ne 0 ]; then
		echo "Error configuring zlib for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building zlib crosstool for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	if [ "${_GLIBC_VERSION}" = "2_31" ]; then
		echo -e "[+] openSSL 1.1.1\r\n"
		cd openssl_1_1_1
	else
		echo -e "[+] openSSL\r\n"
		cd openssl
	fi
	#en mips hay que desactivar los threads con no-threads
	./Configure ${_OPENSSL_TARGET} --prefix=${DSTDIR} no-shared zlib-dynamic -I${_CT_DIR}/.build/${TARGET}/buildtools/include 
	if [ $? -ne 0 ]; then
		echo "Error configuring openssl for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU} CC="${cross}gcc" AR="${cross}ar" RANLIB="${cross}ranlib"
	if [ $? -ne 0 ]; then
		echo "Error building openssl crosstool for ${TARGET}"
		exit
	fi
	make install
	
	#cp -RL include/openssl ${OUTPUT_INCLUDE}
	#cp libcrypto.so ${OUTPUT_LIB}
	#cp libcrypto.a ${OUTPUT_LIB}
	#cp libssl.so ${OUTPUT_LIB}
	#cp libssl.a ${OUTPUT_LIB}
	
	cd ${_LIB_DIR}
	
	echo -e "[+] curl\r\n"
	cd curl
	autoreconf -fi
	if [ $? -ne 0 ]; then
		echo "Error setting up libcurl for ${TARGET}"
		exit
	fi
	#--disable-threads
	./configure --prefix=${DSTDIR} --target=${TARGET} --host=x86_64-pc-linux-gnu --with-zlib=${DSTDIR} --with-ssl=${DSTDIR} --enable-debug --disable-optimize --disable-symbol-hiding --disable-ftp --disable-ldap --disable-ldaps --disable-rtsp --disable-telnet --disable-proxy --disable-tftp --disable-pop3 --disable-imap --disable-rtsp --disable-smb --disable-smtp --disable-gopher --disable-mqtt --disable-manual --disable-threaded-resolver --disable-pthreads --disable-ntlm --disable-ntlm-wb --disable-doh --disable-dnsshuffle --disable-hsts --disable-websockets --without-huper --without-brotli --without-zstd --without-libpsl --without-librtmp --without-winidn --without-libidn2 --without-quiche --without-msh3 
	if [ $? -ne 0 ]; then
		echo "Error configuring libcurl for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building libcurl for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] libnl\r\n"
	cd libnl
	./autogen.sh
	if [ $? -ne 0 ]; then
		echo "Error setting up libnl for ${TARGET}"
		exit
	fi
	./configure --prefix=${DSTDIR} --host=x86_64-pc-linux-gnu
	if [ $? -ne 0 ]; then
		echo "Error configuring libnl for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building libnl for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] libarchive\r\n"
	cd libarchive/build
	./autogen.sh --prefix=${DSTDIR} --host=x86_64-pc-linux-gnu --target=${TARGET} 
	if [ $? -ne 0 ]; then
		echo "Error setting up libarchive for ${TARGET}"
		exit
	fi
	cd ..
	./configure  --prefix=${DSTDIR} --host=x86_64-pc-linux-gnu --target=${TARGET} --without-xml2 --without-bz2lib --without-libb2 --without-iconv --without-lz4 --without-zstd --without-lzma --without-cng --with-mbedtls --with-nettle --without-openssl
	if [ $? -ne 0 ]; then
		echo "Error configuring libarchive for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building libarchive for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] json-c\r\n"
	cd json-c
	if [ ! -d "build" ]; then
		mkdir build
	fi
	cd build
	#clean previous content
	#We need to check this because the script is launched with -e
	if [ $(ls | wc -l) -ne 0 ]; then
		rm -r *
	fi
	../cmake-configure --prefix=${DSTDIR}
	if [ $? -ne 0 ]; then
		echo "Error configuring json-c for ${TARGET}"
		exit
	fi
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building json-c for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	echo -e "[+] nDPI\r\n"
	cd nDPI
	./autogen.sh  --prefix=${DSTDIR} --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --with-only-libndpi
	if [ $? -ne 0 ]; then
		echo "Error setting up libndpi for ${TARGET}"
		exit
	fi
	./configure  --prefix=${DSTDIR} --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --with-only-libndpi
	if [ $? -ne 0 ]; then
		echo "Error configuring libndpi for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building libndpi for ${TARGET}"
		exit
	fi
	make install
	
	cd ${_LIB_DIR}
	echo "[+] Building libUSB"
	cd libusb
	make clean
	./autogen.sh  --prefix=${DSTDIR} --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --enable-udev=no 2>&1 >/dev/null
	./configure --prefix=${DSTDIR}  --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET} --enable-udev=no 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	sudo make install 2>&1 >/dev/null
	echo -e "\t[+] libUSB built!"
	
	cd ${_LIB_DIR}
	echo -e "[+] GSL\r\n"
	cd gsl
	./autogen.sh --prefix=${DSTDIR}  --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=x86_64-pc-linux-gnu --target=${TARGET}
	if [ $? -ne 0 ]; then
		echo "Error setting up libgsl for ${TARGET}"
		exit
	fi
	./configure --prefix=${DSTDIR} --libdir=${DSTDIR}/lib --includedir=${DSTDIR}/include --host=aarch64-rpi4-linux-gnu CCFLAGS="-march=armv8-a -mfloat-abi=hard"
	if [ $? -ne 0 ]; then
		echo "Error configuring libgsl for ${TARGET}"
		exit
	fi
	make clean
	make -j${_NUM_CPU}
	if [ $? -ne 0 ]; then
		echo "Error building libgsl for ${TARGET}"
		exit
	fi
	sudo make install
	
	cd ${_LIB_DIR}
	
	cd ${_CT_DIR}	
	echo -e "[+] Copying build to destination dir\r\n"
	mkdir -p ${_FINAL_DST_DIR}/${DST_TOOLCHAIN_PATH}/toolchain
	mkdir -p ${_FINAL_DST_DIR}/${DST_TOOLCHAIN_PATH}/libs
	sudo cp -vaL .build/${TARGET}/buildtools/* ${_FINAL_DST_DIR}/${DST_TOOLCHAIN_PATH}/toolchain
	sudo cp -vaL ${DSTDIR}/* ${_FINAL_DST_DIR}/${DST_TOOLCHAIN_PATH}/libs
	
	echo -e "[+] Toolchain available at ${_FINAL_DST_DIR}\r\n"
	cd ${_PWD}
	
}

function buildNDK() {
	cd ${_PWD}/${_NDK_VERSION}
	
	export DST_TOOLCHAIN_PATH=${TARGET}
	export ANDROID_NDK_HOME=$(pwd)
	export ANDROID_NDK_ROOT=$(pwd)
	export toolchains_path=$(python toolchains_path.py --ndk ${ANDROID_NDK_HOME})
	# Set compiler clang, instead of gcc by default
	export CC=clang
	# Set the Android API levels
	export ANDROID_API=21
	# Set the target architecture
	# Can be android-arm, android-arm64, android-x86, android-x86 etc
	export CROSS_COMPILE=aarch64-linux-android
	export CC=aarch64-linux-android26-clang
	export architecture=android-arm64
	# Add toolchains bin directory to PATH
	export PATH=$toolchains_path/bin:$PATH
	OUTPUT=$ANDROID_NDK_HOME/built-libs/
	OUTPUT_INCLUDE=$ANDROID_NDK_HOME/built-libs/include
	OUTPUT_LIB=$ANDROID_NDK_HOME/built-libs/lib/${architecture}
	mkdir -p $OUTPUT_INCLUDE
	mkdir -p $OUTPUT_LIB
	
	cd ${_LIB_DIR}
	
	echo "[+] Building zLib..."
	cd zlib
	./configure --prefix=${OUTPUT} 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	echo -e "\t[+] zLib built!"
	cd ${_LIB_DIR}
	
	echo "[+] Building OpenSSL"
	cd openssl
	./Configure ${architecture} --prefix=${OUTPUT} -D__ANDROID_API__=$ANDROID_API 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	cp -RL include/openssl $OUTPUT_INCLUDE 2>&1 >/dev/null
	cp libcrypto.so $OUTPUT_LIB 2>&1 >/dev/null
	cp libcrypto.a $OUTPUT_LIB 2>&1 >/dev/null
	cp libssl.so $OUTPUT_LIB 2>&1 >/dev/null
	cp libssl.a $OUTPUT_LIB 2>&1 >/dev/null
	echo -e "\t[+] OpenSSL Built!"
	
	cd ${_LIB_DIR}

	echo "[+] Building libcURL"
	cd curl
	autoreconf -fi 2>&1 >/dev/null
	./configure --prefix=${OUTPUT} --target=${architecture} --with-zlib=${OUTPUT} --with-ssl=${OUTPUT} --host=x86_64-pc-linux-gnu 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	echo -e "\t[+] libcURL built!"
	
	cd ${_LIB_DIR}
	
	echo "[+] Building libnl"
	cd libnl
	./autogen.sh 2>&1 >/dev/null
	./configure --prefix=${OUTPUT} --host=aarch64-linux-android CC=aarch64-linux-android26-clang --disable-pthreads 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	echo -e "\t[+] libnl built!"
	
	cd ${_LIB_DIR}

	echo "[+] Building libArchive"
	cd libarchive/build
	./autogen.sh --prefix=${OUTPUT} --host=aarch64-linux-android --target=${architecture}  2>&1 >/dev/null
	cd .. 2>&1 >/dev/null
	./configure  --prefix=${OUTPUT} --host=aarch64-linux-android --target=${architecture} --without-xml2 2>&1 >/dev/null
	cp -va contrib/android/include/* libarchive/ 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	echo -e "\t[+] libArchive built!"
	
	cd ${_LIB_DIR}
	
	echo "[+] Building json-c"
	cd json-c
	if [ ! -d "build" ]; then
		mkdir build
	else
		rm -r build/*
	fi
	cd build
	../cmake-configure --prefix=${OUTPUT} 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	make install 2>&1 >/dev/null
	echo -e "\t[+] json-c built!"
	
	cd ${_LIB_DIR}
	
	echo "[+] Building nDPI"
	cd nDPI
	./autogen.sh  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} --with-only-libndpi 2>&1 >/dev/null
	./configure  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} --with-only-libndpi 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	sudo make install 2>&1 >/dev/null
	echo -e "\t[+] nDPI built!"
	
	cd ${_LIB_DIR}
	
	echo "[+] Building libUSB"
	cd libusb
	./autogen.sh  --prefix=${OUTPUT} --host=x86_64-pc-linux-gnu --enable-udev=no 2>&1 >/dev/null
	./configure  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} --enable-udev=no 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	sudo make install 2>&1 >/dev/null
	echo -e "\t[+] libUSB built!"
	
	cd ${_LIB_DIR}
	
	echo "[+] Building GSL"
	cd gsl
	./autogen.sh  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} 2>&1 >/dev/null
	./configure  --libdir=${OUTPUT}/lib --includedir=${OUTPUT}/include --host=x86_64-pc-linux-gnu --target=${architecture} 2>&1 >/dev/null
	make clean 2>&1 >/dev/null
	make -j${_NUM_CPU} 2>&1 >/dev/null
	sudo make install 2>&1 >/dev/null
	echo -e "\t[+] GSL built!"
	
	cp -a ${_PWD}/${_NDK_VERSION} ${_FINAL_DST_DIR}/android_x64
	
	echo "Toolchain written to ${_OUT_DIR}"
	
}

if [ -z $1 ]; then
	echo "No toolchain provided"
	exit 1
fi

getSources



if [ "$1" == "android_x64" ]; then
	buildNDK
elif [ "$1" == "mipsel-buildroot-linux-uclibc" ]; then
	buildMIPSEL_uClibc
else
	if [ -z $2 ]; then
		echo "No openssl platform provided"
		exit 1
	fi
	buildCrossTool $1 $2
fi	
	

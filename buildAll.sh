#!/bin/bash
#rpi4 x64
echo "Building for rpi4 x64"
/bin/bash -e ./build.sh aarch64-rpi4-linux-gnu linux-generic64
#arm x64
echo "Building for arm x64 & rpi5"
/bin/bash -e ./build.sh aarch64-unknown-linux-gnu linux-generic64
echo "Building for x64 glibc 2.40"
/bin/bash -e ./build.sh x86_64-unknown-linux-gnu_glibc2_40 linux-generic64

#rpi4 x32
#echo "Building for rpi4 x32"
#/bin/bash -e ./build.sh armv8-rpi4-linux-gnueabihf linux-generic32
#rpi3 x32
#echo "Building for rpi3 x32"
#/bin/bash -e ./build.sh armv8-rpi3-linux-gnueabihf linux-generic32
#rpi3 x64
#echo "Building for rpi3 x64"
#/bin/bash -e ./build.sh aarch64-rpi3-linux-gnu linux-generic64
#arm x64
echo "Building for arm x64"
/bin/bash -e ./build.sh aarch64-unknown-linux-gnu linux-generic64
#mips BE
echo "Building for mips BigEndian"
/bin/bash -e ./build.sh mips-unknown-linux-uclibc linux-mips32 2_31 1
#mips LE
echo "Building for mips LittleEndian"
/bin/bash -e ./build.sh mipsel-unknown-linux-gnu linux-mips32
#mips LE
echo "Building for mips LittleEndian uclibc"
/bin/bash -e ./build.sh mipsel-buildroot-linux-uclibc linux-mips32
#Android
echo "Building for Android x64"
/bin/bash -e ./build.sh android_x64
#x64
echo "Building for x64"
/bin/bash -e ./build.sh x86_64-unknown-linux-gnu linux-generic64 2_40
